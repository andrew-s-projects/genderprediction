#pragma once
#include <iostream>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <math.h>

#define CONVERGENCE 0.001 
#define m_value 1/3 
#define M_E 2.71828
//#define alpha 1

struct theta_values {

	double theta0;
	double theta1;
	double theta2;

};
class Data {

	
public:

	double gender;
	double height;
	double weight;

	Data(double Gender, double height, double weight) {
		gender = Gender;
		this->height = height;
		this->weight = weight;
	}

	// for debugging
	void print_members() {
		std::cout << gender << " " << height << " " << weight <<'\n';
	}
};


inline void LoadData(std::vector<Data> &data, std::string);
inline std::vector <std::string> parseData(std::string);
inline double cost_function(struct theta_values &theta, std::vector<Data> samples);
inline void gradient_descent(theta_values &theta, std::vector<Data>);
inline double logistic(double theta);
void test_function(std::vector<Data>, theta_values theta);