
#include "LinearRegresssion.h"


int main()
{
	std::vector<Data> data;
	std::vector<Data> gueses;
	LoadData(data, "training data.txt");
	LoadData(gueses, "testing data.txt");

	
	theta_values iteration;
	//supply initial theta values
	iteration.theta0 = .6868;
	iteration.theta1 = .2331;
	iteration.theta2 = .9884;

	/*
	1) Continually minimize parameters 
	2) Recompute cost function after gradient_descent to see if there's convergence
	*/

	std::vector<Data> temp;
	int i = 0;
	while (data.size() != 0) {
		
		for (int j = 0; j < 4; j++) {
			Data element = data[0];
			data.pop_back();
			temp.push_back(element);

		}
		//continually minimize cost function until convergence
		gradient_descent(iteration, temp);
		
		//if function has converged, break
		if (cost_function(iteration, temp) < CONVERGENCE) {
			break;
		}

	}
	test_function(gueses, iteration);
	

}

//minimizes the parameters based on gradient_descent procedure
void gradient_descent(theta_values &theta, std::vector<Data> samples) {

	/*

	Calculate Gradient Descent iteration by adding all these parts together:
	1) Dimension of model parameter * partial derivative of theta(0) 
	2) Constant Value theta(1)
	3) Constant Value theta(2)

	*/
	
	//theta values through iteration
	double theta0_h = 0;
	double theta1_h = 0;
	double theta2_h = 0;
	double alpha = 0.01;

	
	for (int i = 0; i < samples.size(); i++) {
		

		theta0_h += (theta.theta0 + theta.theta1 * samples[i].height + theta.theta2 * samples[i].weight - samples[i].gender);
		theta1_h += (theta.theta0 + theta.theta1 * samples[i].height + theta.theta2 * samples[i].weight - samples[i].gender) * samples[i].height;
		theta2_h += (theta.theta0 + theta.theta1 * samples[i].height + theta.theta2 * samples[i].weight - samples[i].gender) * samples[i].weight;
		
	}

	//values for next iteration

	theta.theta0 = theta.theta0 - (1.0 / 3.0) * theta0_h;
	theta.theta1 = theta.theta1 - (1.0 / 3.0) * theta1_h;
	theta.theta2 = theta.theta2 - (1.0 / 3.0) * theta2_h;


}

//minimizes parameters based on cost function
double cost_function(struct theta_values &theta, std::vector<Data> samples) {

	
	double theta0_h = 0;
	double theta1_h = 0;
	double theta2_h = 0;

	//use minimized theta values to recompute cost function and check for convergence

	for (int i = 0; i < samples.size(); i++) {
		
		theta0_h += samples[i].gender * log(logistic(theta.theta0)) + (1 - samples[i].gender) * (log(1 - logistic(theta.theta0)));
		theta1_h += samples[i].gender * log(logistic(theta.theta1)) + (1 - samples[i].gender) * (log(1 - logistic(theta.theta1)));
		theta2_h += samples[i].gender * log(logistic(theta.theta2)) + (1 - samples[i].gender) * (log(1 - logistic(theta.theta2)));

	}

	return (-1 / 3) * (theta0_h + theta1_h + theta2_h);

	

}

//computes logistic function value
double logistic(double theta) {

	return 1/(1 + pow(M_E, -theta));
	

}

void LoadData(std::vector<Data> &data, std::string the_file) {

	std::ifstream fileStream(the_file);

	
	if (!fileStream) {
		std::cout << "Unable to open file ";

	}
	else {
		std::string line;
		std::vector <std::string>parsed_data;
		while (getline(fileStream, line)) {

			parsed_data = parseData(line);
			Data sample(std::stod(parsed_data[0]), std::stod(parsed_data[1]), std::stod(parsed_data[2]));
			data.push_back(sample);
		}
		fileStream.close();
	}

}

//attempts to compute correct values of test data
void test_function(std::vector<Data>values, theta_values theta) {
	//guess values for each sample

	int correct_number = 0; //correct guesses
	double total_correct = 0;

	for (int i = 0; i < values.size(); i++) {
		double guess = theta.theta0 + theta.theta1 * values[i].height + theta.theta2 * values[i].weight;

		//correct guess
		if (guess == values[i].gender) {
			correct_number++;
		}
	}
	total_correct = correct_number / values.size();

	std::cout << "Percent guessed correctly " << total_correct * 100;

}



//parses incoming training and testing files
std::vector<std::string>parseData(std::string input) {

	std::string deliminter = ",";
	size_t pos = 0;
	std::string item;
	std::vector<std::string> parsed_data;

	while ((pos = input.find(deliminter)) != std::string::npos){
		item = input.substr(0, pos);
		
		if (item == "\"Male\"") {
			parsed_data.push_back("0.0");
		}
		else if (item == "\"Female\"") {
			parsed_data.push_back("1.0");
		}
		else {
			parsed_data.push_back(item);
		}
		input.erase(0, pos + deliminter.length());
		}
	parsed_data.push_back(input);
	//std::cout << input;
	return parsed_data;
}
